ARG PYTHON_VERSION
# use for example --build-arg PYTHON_VERSION=3.10-slim
FROM python:$PYTHON_VERSION

ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1


RUN apt update \
 && apt install -y --no-install-recommends \
 xmlsec1 libxmlsec1-dev \
 libsqlite3-0 \
 locales \
 git \
 && sed -i 's/^# *\(en_US.UTF-8\)/\1/' /etc/locale.gen \
 && sed -i 's/^# *\(es_AR.UTF-8\)/\1/' /etc/locale.gen \
 && locale-gen \
 && rm -rf /var/lib/apt/lists/*

RUN mkdir /app
WORKDIR /app

ONBUILD COPY install-prereqs*.sh Pipfile* requirements* /app/

ONBUILD RUN bash -c " \
    if [ -f '/app/install-prereqs.sh' ]; then \
        bash /app/install-prereqs.sh; \
    fi" \
 && rm -rf /var/lib/apt/lists/*

RUN pip install --upgrade pip

# Install tox and pipenv if needed
ONBUILD RUN bash -c " \
    if [[ -f '/app/Pipfile.lock' || -f '/app/Pipfile' ]]; then \
        pip install tox-pipenv-install; \
    fi" 

#install tox if needed
ONBUILD RUN bash -c " \
    if [[ -f '/app/requirements.txt' ]]; then \
        pip install tox; \
    fi" 


ONBUILD COPY tox.ini /app/
# Build the tox environments without running tox tests so they will be cached 
ONBUILD RUN tox -r --notest 

ONBUILD COPY . /app/

ONBUILD RUN bash -c " \
    if [ -f '/app/install-postreqs.sh' ]; then \
        bash /app/install-postreqs.sh; \
    fi"

ONBUILD RUN bash -c "tox"
ONBUILD RUN bash -c "rm -rf .tox"
